# email-client-admin-lte

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install --save-dev

# run backend in the first CLI
# to access backend you need to go to localhost:3000
npm run backend


# run frontend in the second CLI
# to access frontend you need to go to localhost:8080
npm run frontend
```